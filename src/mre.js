const { hello, world } = require('./mocked')

module.exports = {
    helloWorld: () => `${hello}, ${world}!`,
}
