const { helloWorld } = require('./mre')

const mockHello = 'Hi'
const mockWorld = 'Example'

jest.mock('./mocked', () => ({
    hello: mockHello,
    world: mockWorld,
}))

describe('Minimum reproducible example', () => {
    it("doesn't work", () => {
        const result = helloWorld()
        expect(result).toBe('Hi, Example!')
    })
})
